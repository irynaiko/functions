const getSum = (str1, str2) => {
  if(typeof(str1) === 'string' && typeof(str2) === 'string' && !isNaN(str1) && !isNaN(str2)) {
    let num = +str1 + +str2;
    return num.toString();
  } else {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = [];
  let comments = [];
  listOfPosts.forEach(el => {
    if(el.author == authorName) {
      if(el.post) {
        post.push(el.post)
      }
    }
    if(el.comments) {
      el.comments.forEach(element => {
        if(element.author == authorName) {
          comments.push(element.comment);
        }
      })
    }
  });
  return `Post:${post.length},comments:${comments.length}`
};

const tickets=(people)=> {
  let cashBox = {
    25:0,
    50:0,
    100:0
  }
  
  for (let i = 0 ; i < people.length ; i++){
  
    if(people[i] === 25){
      cashBox[25]++;
    }
    
    else if(people[i] === 50){
      if(cashBox[25] !== 0){
        cashBox[25]--;
        cashBox[50]++;
      }
      else return "NO";
    }
    
    else if(people[i] === 100){
      if(cashBox[25] !== 0 && cashBox[50] !== 0){
        cashBox[25]--;
        cashBox[50]--;
        cashBox[100]++;
      }
      else if(cashBox[25] > 2){
        cashBox[25] = cashBox[25] - 3;
        cashBox[100]++;
      }
      else return "NO";
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
